#include<stdio.h>
#include<math.h>
float x_coordinates(float x1, float x2)
{
 return((x1-x2)*(x1-x2));
}
float y_coordinates(float y1, float y2)
{
return ((y1-y2)*(y1-y2));
}
float result(float x1, float x2, float y1, float y2)
{
float part_1=x_coordinates(x1,x2);
float part_2=y_coordinates(y1,y2);
float result= sqrt(part_1+part_2);
return result;
}
int main()
{
float x1,x2,y1,y2 ,distance=0;
printf("Enter x1 value:\n");
scanf("%f",&x1);
printf("Enter y1 value:\n");
scanf("%f", &y1);
printf("Enter x2 value:\n");
scanf("%f",&x2);
printf("Enter y2 value:\n");
scanf("%f", &y2);
distance=result(x1,x2,y1,y2);
printf("The distance is: %f", distance);
return 0;
}
