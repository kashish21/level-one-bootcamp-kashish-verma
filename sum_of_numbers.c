#include<stdio.h>
int input()
{
int number;
printf("Enter the number of elements to be added:\n");
scanf("%d", &number);
return number;
}
void input_array(int n, int a[n])
{
printf("Enter the elements of the array:\n");
for(int i=0; i<n;i++)
{
scanf("%d",&a[i]);
}
}
int compute(int n, int a[n])
{
int sum=0;
for(int i=0; i<n;i++)
{
sum=sum+a[i];
}
return sum;
}
int output(int sum)
{
printf("The sum of the given elements is=%d\n",sum);
return sum;
}
int main()
{
int n,sum;
n=input();
int a[n];
input_array(n,a);
sum=compute(n,a);
output(sum);
}