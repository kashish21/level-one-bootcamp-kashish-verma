
#include<stdio.h>
typedef struct fraction
{
int num, den;
}Fraction;
Fraction input()
{
Fraction f;
printf("Enter the numerator:\n");
scanf("%d",&f.num);
printf("Enter the denominator:\n");
scanf("%d",&f.den);
return f;
}
int hcf(Fraction r)
{
int i, gcd;
for(i=1;i<=r.num && i<=r.den; ++i)
{
if(r.num%i==0 && r.den%i==0)
gcd=i;
}
return gcd;
}
Fraction sum(Fraction f1, Fraction f2)
{
int gcd;
Fraction temp;
Fraction r;
temp.num=((f1.num*f2.den)+(f2.num*f1.den));
temp.den=(f1.den*f2.den);
gcd=hcf(temp);
r.num=temp.num/gcd;
r.den=temp.den/gcd;
return r;
}
void result(Fraction f1, Fraction f2, Fraction r)
{
printf("The sum of %d/%d+%d/%d is %d/%d", f1.num, f1.den, f2.num, f2.den, r.num, r.den);
}
int main()
{
Fraction f1,f2,res;
printf("Enter the numerator and denominator of the first fraction:");
f1=input();
printf("Enter the numerator and denominator of the second fraction:");
f2=input();
res=sum(f1,f2);
result(f1,f2,res);
return 0;
}

